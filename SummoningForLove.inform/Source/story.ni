"Summoning for Love" by Sen Cantor

Release along with the "Quixe" interpreter.

Include Rideable Vehicles by Graham Nelson. 
Include Basic Help Menu by Emily Short.

Part 0 - Setup for the Game

Chapter 1 - Some Options for the Player

scenebrevity is a kind of value. The scenebrevities are all, once, skip, and skipall.
The current brevity is a scenebrevity that varies. The current brevity is usually all.

Scenesetting is an action applying to a scenebrevity.
Understand "scenesetting [a scenebrevity]" as scenesetting.

Carry out scenesetting a scenebrevity:
	now the current brevity is the scenebrevity understood.

Report scenesetting:
	say "Set."

A person can be interacted or uninteracted. A person is usually uninteracted.

Catching up is an action applying to nothing.
Understand "catch up" as catching up.

Check catching up:
	if ritual state is not 0:
		say "Sorry, catching up is only available at the beginning of the game." instead.

Carry out catching up:
	try drawing;
	try salting;
	try placing candles;
	try lighting;
	try placing cucumber;
	try xyzzy;
	try going east;
	try talking to guard;
	try giving desire energy to guard;
	try going east;
	try going north;
	try talking to the quiet reader;
	try giving pleasure to the quiet reader;
	try going south;
	try inserting the wooden key into the little storage box;
	try resting;
	try taking the wooden key;
	try going east;
	try talking to the swordfighter;
	try giving pleasure to the swordfighter;
	try going west;
	try going north;
	try going north;
	try going north;

Chapter 2 - Setup

A thing can be seen or unseen.

Carry out examining:
	Now the noun is seen.

To say fixed: say fixed letter spacing.
To say var: say variable letter spacing.
To say italic: say italic type.
To say bold: say bold type.
To say normal: say roman type.
To say emdash: say "[unicode 8212]"

Chapter 3 - Debugging - Not for release

When play begins (this is the run property checks at the start of play rule): 
	repeat with item running through things: 
		if description of the item is "": 
			say "[item] has no description.";
	repeat with item running through rooms: 
		if description of the item is "": 
			say "[item] has no description." 

Part 1 - Summoning Ritual

When play begins:
	say "Well, it's almost time. You've decided that you're going to get yourself a girlfriend, come hell or high water, but mainly hell. Sure, summoning a succubus sounds dangerous, but falling in love with someone is [italic]kind[normal] of like giving your soul to them anyway, right? You've made a space in your attic, and brought all of the components needed, plus the tome[emdash]or, well, instructional pamphlet borrowed from the library, at least[emdash]describing how to summon a sexy succubus. The instructions didn't say to wait till midnight, but everything's more magical at this time of night, so it can't hurt, right? It's not at all because you're nervous.[paragraph break]Yup. Any minute now. Just...follow the instructions. Easy.";
	choose row 1 in Table of Basic Help Options;
	now description entry is "Hello! Here is a brief primer on commands unique to this game.[paragraph break]'talk to (NPC)' allows you to strike up a conversation with an NPC. Most NPCs in this game will want something from you, so talking to them will help you learn what they want.[paragraph break]'give (item) to (NPC)' will let you give an NPC what they want. Most NPCs will want sexual energy of some kind. If, for example, the gate guard wants desire energy, then 'give desire to guard' will work. Don't worry about the amount; if you have enough, you will give that much automatically. Giving NPCs sexual energy will typically trigger a sex scene.[paragraph break]'rest' is a command that will allow you to recover sexual energy. You can only rest at designated spots, which will be explicitly identified as resting areas. It will also reset most of the current puzzle; doors will be relocked, NPCs will take back the items they gave you, etc. There is one crucial exception, though! Most resting areas will have a box where you can place a number of items, and those items will stay right where they are. This will be vital to solving puzzles![paragraph break]There are also some settings to allow you to skip scenes if you wish, using the 'scenesettings' command. 'scenesettings all' is the default, which will show all conversations and sex scenes. 'scenesettings once' will only show you a sex scene the first time. They don't change on repeats, so don't worry, you won't miss anything. 'scenesettings skip' will always skip sex scenes. If there's a character or sex act you're not comfortable with, you may wish to set this temporarily. 'scenesettings skipall' will skip all conversations and sex scenes. I'm not sure I'd recommend this, but I'll try to make sure you get the gist of what's going on."

The description of the player is "You're a short, plump little raccoon lady with dusty brown fur. You've taken off all your clothes[if the player is in the attic][emdash]you remember, the ritual needs you to be naked, right?[emdash][otherwise], [end if]so your round little breasts are quite visible, as well as the thick, short dick and heavy balls tucked between your legs."

Attic is a room. The printed name of the attic is "Attic". The player is in the attic.  The description of the attic is "The attic is lit by a dim overhead bulb. The floors are bare hardwood, and there are various boxes stacked around the room, which you have pushed to the side to make room for the ritual.[if the ritual state is greater than 0][paragraph break]You've now got a rather large symbol drawn on the floor, enclosed by an eight foot diameter circle.[end if][if the ritual state is greater than 1] The symbol has now been enclosed in salt. Hopefully the seal works right.[end if][if the ritual state is greater than 2] A candle has been placed in each of the designated locations in the circle.[end if][if the ritual state is greater than 3] Each one bears a tiny flickering flame.[end if][if the ritual state is greater than 4] And, of course, the cucumber sits in the center, looking decidedly less magical than everything else.[end if][if unvisited][paragraph break](Type 'help' for an introduction to this game's mechanics and basic info on interactive fiction.)[paragraph break](If you're actively following development of this game, you might be interested in only seeing what's new. If you want to automatically jump ahead to the latest content, just type 'catch up' before inputting any other commands. If this is your first time playing the game, just ignore this, you'll end up missing a lot!)[end if]"

The light bulb is scenery in the attic. The description of the light bulb is "Just a naked bulb hanging from the ceiling. Conveniently, it's bright enough to see and read by, but still dim enough to provide a spooky atmosphere. You're not sure it would have the same effect if the attic was bright and well-lit."

The floor is scenery in the attic. Understand "floors" as the floor. The description of the floor is "Ah, I see you're a sharp one. You saw me describing a floor and thought to yourself 'Ah, I've got you now! If there really is a floor in this room, why, show it to me, rather than giving me some generic reply when I try to look at it!' And I feel you. But let's both be honest, you didn't [italic]actually[normal] think there was anything interesting to see about the floor, did you?"

The boxes are scenery in the attic. The description of the boxes is "You're pretty sure that when you first moved here, about a quarter of the boxes were things you'd never opened before, didn't bother to look in, and immediately stuffed them in the attic and never thought about them again. ...now you're mildly curious, but considering what you're in the middle of, you're not keen to set multiple horror movie plots in motion at once. Probably best to focus on the ritual."

A thing called the chalk is carried by the player. The description of the chalk is "Just regular white chalk. You haven't used chalk for anything in years. Everyone just uses erasable markers instead. You can't be certain that erasable markers wouldn't work, but finding a giant whiteboard floor sounds pretty difficult, so chalk will do."

A thing called the summoning salt is carried by the player. The description of the summoning salt is "Okay, so it's just a container of regular salt. But any salt can be summoning salt if you use it right. It also has nothing to tell you about speedrunning, unfortunately."

A thing called the candles are carried by the player. The description of the candles is "Six candles, from the local scented candle store. You weren't sure which scent would be appropriate for a demon summoning. Something 'Halloweeny' was the closest you could get, so you hope this demon likes Autumn Spice.[if the ritual state is greater than 3] Each one now has a cheery little flame flickering at the tip of its wick, and you have to admit, they do smell like, uh, spice.[end if]"

A matchbox is carried by the player. The description of the matchbox is "A box of matches, for lighting the candles. You're very concerned about fire safety and mildly nervous about handling the matches, but we all need to take risks for love. Especially extremely mild risks."

A thing called the cucumber is carried by the player. The description of the cucumber is "You suppose a cucumber is kind of like a dildo. That's not a viewpoint that you have personally endorsed, but it was easy to get ahold of, and the succubus you intend to summon must like them. Maybe you'll get to see her, um, put it somewhere."

A thing called the instructional pamphlet is in the attic. Understand "tome" as the pamphlet. The description of the pamphlet is "Okay, let's see here...[paragraph break][fixed]So you want to summon a succubus? Well it's easy! First, you're going to want to find a nice open space where you can draw on the floor, about eight feet in diameter. Mark the circle in chalk, and then [bracket]draw[close bracket] this [bracket]symbol[close bracket] inside it.[paragraph break][var](a dizzying array of glyphs and lines is printed inside the pamphlet.)[paragraph break][fixed]Got it? Good! Now [bracket]salt[close bracket] the original [bracket]circle[close bracket]. It's EXTREMELY IMPORTANT that there are no breaks or smears in the line. Double check before you continue![paragraph break]Next, [bracket]place candles[close bracket] in the designated locations on the summoning circle. You'll need six in all. [bracket]Light[close bracket] each one.[paragraph break]Last of all, you'll need to select an item of power that corresponds to the demon you want to summon.[paragraph break][var](a very long and kind of confusing table listing different demons and their items of power is printed here. For your ritual, you picked a cucumber, which should qualify as a 'suggestive fruit or vegetable'.)[paragraph break][fixed][bracket]Place[close bracket] the item in the center of the circle. Make absolutely sure your arms and legs are outside the circle, and then speak the word of summoning: [bracket]xyzzy[close bracket]![paragraph break]And that's it! Happy summoning![var]"

Ritual state is a number that varies. Ritual state is 0.

Drawing is an action applying to nothing.
Understand "draw symbol" as drawing.
After drawing:
	If the player is in the attic:
		If the ritual state is 0:
			say "You painstakingly recreate the symbol printed in the pamphlet. You don't want to mess this up, so you spend a good half an hour comparing your drawing to the original. Who knows what would happen if you got a detail wrong. Well, no one said this would be easy. Oh wait. The pamphlet literally said that. Guess it's like they say: never trust a pamphlet.";
			Increase the ritual state by 1;
			Remove the chalk from play;
		otherwise:
			say "I know it was [italic]so[normal] much fun the first time, but there's no need to draw the dumb thing again.";
	otherwise:
		say "There's no need to draw anything here."

Salting is an action applying to nothing.
Understand "salt circle" as salting.
After salting:
	If the player is in the attic:
		If the ritual state is 1:
			say "You take the salt container and pour a thick line over the chalk circle. Better safe than sorry, although it does end up leaving an awful lot of salt on the floor. By the time you're done, the container is quite empty. You hope the salt will be consumed by the ritual somehow; this will be a lot to clean up.";
			Increase the ritual state by 1;
			Now the salt is in the attic;
			Now the salt is scenery;
			Now the description of the salt is "Salt, now poured on the floor. Are you sure this is a good idea, actually?";
		otherwise if the ritual state is less than 1:
			say "Hold on, don't go dumping salt all over the ground just yet. You need to draw the symbol first.";
		otherwise:
			say "You're all out of salt. There's plenty on the floor anyway, no need to add more.";
	otherwise:
		say "There's no need to mess around with salt here, even if you still had any."

Placing candles is an action applying to nothing.
Understand "place candles" as placing candles.
After placing candles:
	If the player is in the attic:
		If the ritual state is 2:
			say "You walk around the edge of the salt circle and place each candle in its designated little circle. Well, now this is really shaping up to look like a proper ritual circle. You're even starting to seriously think this will actually work.";
			Increase the ritual state by 1;
			Now the candles are in the attic;
			Now the candles are scenery;
		otherwise if the ritual state is less than 2:
			say "Hold on, gotta do this in the right order. Chalk symbol, then salt circle, then candles. It'd be a lot more convenient to do the salt last, but you're not going to disobey a pamphlet, are you?";
		otherwise:
			say "The candles are all neatly in place. No need to move them around.";
	otherwise:
		say "You don't really have any candles left to play with at this point."

Lighting is an action applying to nothing.
Understand "light candles" as lighting.
After lighting:
	If the player is in the attic:
		If the ritual state is 3:
			say "You strike a match, and carefully light each one in turn. Halfway through you get nervous about the matchstick burning down, so you blow it out and strike another one. Can't be too careful, after all. That done, you put the matchbox away. Best not to leave it sitting out, after all.";
			Increase the ritual state by 1;
			Remove the matchbox from play;
		otherwise if the ritual state is less than 3:
			say "The candles aren't in place yet, don't go lighting them now.";
		otherwise:
			say "No, you have enough fire right now, that's fine.";
	otherwise:
		say "You left the matchbox back in your attic. There's no going back for it now."

Placing cucumber is an action applying to nothing.
Understand "place cucumber" as placing cucumber.
Understand "place item" as placing cucumber.
After placing cucumber:
	If the player is in the attic:
		If the ritual state is 4:
			say "You carefully step over the salt circle and place the cucumber gingerly in the middle of the circle. With everything in place, you regard the phallic vegetable with great care, as if it's a bomb that might go off. Not turning your eyes away from it, you back out of the circle. Nothing should happen till you say the magic word...right, what was it, again?";
			Increase the ritual state by 1;
			Now the cucumber is in the attic;
			Now the cucumber is scenery;
		otherwise if the ritual state is less than 4:
			say "Not yet, the cucumber goes in last. The circle, I mean. It goes in the circle last.";
		otherwise:
			say "You'd rather not step back in the circle again. Everything's set up, you just need that magic word.";
	otherwise:
		say "If you're thinking of phallic objects, there are better options at this point."

Xyzzy is an action applying to nothing.
Understand "xyzzy" as xyzzy. Understand "say xyzzy" as xyzzy. Understand "speak xyzzy" as xyzzy.
After xyzzy:
	If the player is in the attic:
		If the ritual state is 5:
			say "Just as the word leaves your lips, you catch something out of the corner of your eye. Your gaze flicks to the circle, and you see a single pawprint on the edge of the circle. Wait...were you paying attention to your feet when you stepped out of the circle? You barely have time to process your mistake before the circle is filled with a pillar of purple light. Where your pawprint broke the circle, the light begins to spill out, pouring into the room. You turn to run, but you can feel a force pulling you backwards, towards the circle. You tumble backwards, your vision filling with purple light.[paragraph break]The sensation of movement suddenly stops as your body falls backwards onto something rough and gritty. The salt...? No, there's way too much. As your vision clears, you find yourself standing in some kind of cavern, well lit by ambient purple light. The cave walls extend high up, so high that the ceiling forms a sort of purple-tinged night sky, the texture of the rock only barely visible. You struggle to your feet and look around. It seems like you landed on dust accumulated on the ground, which is admittedly better than landing on rock. Oh yeah, and you're still completely naked...well, there's not much you can do about that. All you can do is look around for a way out. Where on earth is this place, anyway? Did that summoning circle take you to, um, Hell?";
			Remove the instructional pamphlet from play;
			Now the player is in the dusty alcove;
		otherwise:
			say "Don't go saying that yet! You have no idea what will happen if you speak it at the wrong time.";
	otherwise:
		say "Don't go saying that word now! That's what got you into this mess in the first place."

Part 2 - Spirited Away

Chapter 0 - Some Notes on Puzzles

An energy supply is a kind of thing. An energy supply has a number called amount. The amount of an energy supply is usually 0.

Rule for printing the name of an energy supply while taking inventory:
	say "[amount] [printed name]"

Instead of dropping an energy supply:
	say "It's not exactly the sort of thing you can drop."
Instead of inserting an energy supply into something:
	say "It's not exactly the sort of thing you can drop."
Instead of putting an energy supply on something:
	say "It's not exactly the sort of thing you can drop."

desire energy is an energy supply.  The description of desire energy is "Well you can't see it, exactly, but you can kind of feel it, a sort of simmering warmth in your belly, just waiting to be drawn to the surface."

orgasmic energy is an energy supply. The description of orgasmic energy is "It's not something you can observe directly, but you can sense a sort of charge between your legs, ready to spill out with enough coaxing. You know, kind of like cum. Not [italic]exactly[normal] like cum...because it's energy, of course."

pleasure energy is an energy supply. The description of pleasure energy is "It's not visible to the eye, but you know it's there; a will and an excitement to give pleasure to another, poised to flow freely through a touch, a kiss, the feel of your body against theirs..."

A person can be sated or unsated. A person can be met or unmet. A person is usually unmet.

Talking to is an action applying to one thing.
Understand "talk to [something]" as talking.

Check talking to:
	If the noun is not a person:
		say "If you like, but don't expect it to talk back." instead.

Report talking to:
	say "[regarding the noun][They're] not very interested in conversation."

A puzzle item is a kind of thing. Belonging to relates various puzzle items to one thing (called the spot). The verb to belong to means the belonging to relation.

A safe place is a kind of container.

A resting spot is a kind of room. A resting spot has a list of puzzle items called items. A resting spot has a list of doors called doors. A resting spot has a list of people called locals. A resting spot has a number called desire amount. A resting spot has a number called orgasm amount. A resting spot has a number called pleasure amount. A resting spot has text called message.

Resting is an action applying to nothing.
Understand "rest" as resting.

The resting action has a list of things called the removed items.

Setting action variables for resting:
	let the removed items be a list of things.

Check resting:
	If the player is not in a resting spot:
		say "This doesn't seem like a good place to rest." instead.

Carry out resting:
	repeat with current item running through the items of the location of the player:
		If the current item is not in a safe place:
			if the current item is carried by the player:
				add the current item to the removed items;
			Now the current item is in the spot of the current item;
	repeat with current door running through the doors of the location of the player:
		Now the current door is closed;
		Now the current door is locked;
	repeat with current local running through the locals of the location of the player:
		Now the current local is unsated;
	Now the amount of desire energy is the desire amount of the location of the player;
	Now the amount of orgasmic energy is the orgasm amount of the location of the player;
	Now the amount of pleasure energy is the pleasure amount of the location of the player.

Report resting:
	Say "[the message of the location of the player][paragraph break]You awaken feeling rested and refreshed. The cubi may have taken back the things you borrowed from them, though.";
	repeat with current item running through the removed items:
		say "([The current item] has been removed from your inventory.)"

Instead of sleeping:
	try resting.

Before opening a locked door:
	if the matching key of the noun is not nothing and the matching key of the noun is carried by the player:
		try unlocking the noun with the matching key of the noun.

Chapter 1 - Introduction at the Gate

Dusty Alcove is a room. The printed name of the dusty alcove is "Dusty Alcove". The description of dusty alcove is "A quiet little dead end, with dust piled up on the ground which might give a soft landing to anyone who happened to be flung here from another dimension.[paragraph break]To the east you can see a stone wall in the distance."

The dust is scenery in the dusty alcove. The description of the dust is "It's thanks to this stuff that you didn't end up getting a concussion. Now that its role in your story is finished, though, you must admit that there's not much to say about it."

Instead of taking the dust:
	say "Well it's not exactly practical to carry around. Mostly your hands just end up all dusty, so I guess you're carrying [italic]some[normal] of it now, technically. You're welcome."

The night sky is a backdrop. It is in the dusty alcove, the outer gate, the cliffside, the cliff wall, the demon's road, and the tiny cave entrance. Understand "ceiling" as the night sky. The description of the night sky is "A deep, purple, starless night sky looms over your head. Seeing just the faintest impression of the ceiling at the top of the cavern gives you a dizzying sense of impossibly large scale. You wonder what it must be like living under a sky like this all the time."

Outer Gate is a room. The printed name of the outer gate is "Outer Gate". Outer gate is east of dusty alcove. The description of outer gate is "The dusty ground gives way to smooth, cool, polished stone forming a path up to a stone wall that stretches from one side of the chamber to the other, blocking the path further east. An iron gate sits in the center of the wall, [if the iron gate is open]sitting open[otherwise]firmly shut[end if]. While neither the wall nor the gate appear to be in the best shape, your lack of siege weapons, equipment, or clothing of any sort makes that a mildly moot point.[paragraph break]To the west is a dusty little alcove. To the east, through the iron gate, is a dark clearing."

The stone wall is a backdrop. It is in the dusty alcove, the outer gate, and the dark clearing. The description of the stone wall is "It seems poorly maintained and kind of crumbling in places, but it's certainly sturdier than you. A determined climber might be able to find handholds in the worn and damaged portions of the wall, but this isn't that kind of game and you're not that kind of heroine."

The iron gate is east of the outer gate and west of the Dark Clearing. The iron gate is a locked closed door. The iron gate is scenery. The description of the iron gate is "A ramshackle, rusted iron gate. None too impressive, but perhaps the fact that it's facing a dead end is why it doesn't exactly look like it could stand up to an invading army. Perhaps it's more designed to keep out naked raccoons."

The guard is a woman in the outer gate. "A gate guard is here, leaning beside the gate. (try 'talk to guard' to start a conversation with her.)" The printed name is "gate guard". Understand "gate guard" as the guard. The guard is unsated. The description of the guard is "A green-skinned reptilian figure leaning against the wall next to the gate. A leather helmet is perched upon her head, tilted down over her eyes, and she wears a belt around her waist with a few pouches hanging from it, but she's otherwise stark naked. She has smallish breasts and a tall, lanky figure with just enough muscle to suggest that she's supposed to be in shape, but isn't. A narrow slit is tucked between her legs. One arm is curled around a spear that is propped up against the wall beside her. You might say that the intended impression here is 'guarding', but that calling it 'sleeping' would be somewhat more accurate."

Instead of talking to the guard:
	If the guard is unmet:
		if the current brevity is skipall:
			say "(Your sexual energy can now be viewed in your inventory. The gate guard wants 1 desire energy to open the gate.)";
		otherwise:
			say "You carefully approach the gate guard, making an effort to try and cover your exposed cock. You can feel yourself stiffening as you try and mostly fail to not stare at the guard's body. 'Um, h-hello? Where am I?'[paragraph break]The guard awakens with a start, then reaches up and tilts her helmet up to see you. 'Eh? A mortal? About time, guard duty is so boring. Well, cutie, you're in Hell. But don't worry, we're friendly down here.' She runs her tongue slowly over her lips as she looks you up and down. 'You're not going back the way you came, so I expect you'll be wanting me to open the gate. But let me give you some free advice: you're in cubi territory, and everyone's gonna want something from a cutie like you.'[paragraph break]Heat spreads over your cheeks as the guard looks you over. 'Want something from me?'[paragraph break]'You look sharp, I'm sure you're already catching on. It's sexual energy.'[paragraph break]You look down at your feet, blushing harder. 'Ah.' Well you [italic]did[normal] want to meet a succubus...[paragraph break]The guard continues, 'Up on the surface, you probably give the stuff away willy-nilly, but down here you should pay a bit more attention. There's three kinds of energy you can offer. Desire energy can be offered when pleasure and need is building up within you~ It's exhausted when you can't take any more teasing and need release. Orgasmic energy is -- well, that should be self-explanatory, shouldn't it? And pleasure energy is given directly -- you know, helping someone get off themselves. If you focus, you should be able to feel these energies within you already.' She beckons you closer. 'A bit of your desire energy is all I want. Don't worry, it'll be fun.'[paragraph break](Your sexual energy can now be viewed in your inventory.)";
		Now the guard is met;
		Now desire energy is carried by the player;
		Now the amount of desire energy is 1;
		Now orgasmic energy is carried by the player;
		Now the amount of orgasmic energy is 1;
		Now pleasure energy is carried by the player;
		Now the amount of pleasure energy is 1;
	otherwise:
		If the guard is unsated:
			if the current brevity is skipall:
				say "(Your sexual energy can now be viewed in your inventory. The gate guard wants 1 desire energy to open the gate.)";
			otherwise:
				say "You ask, 'Sorry, I need to do what?'[paragraph break]'Just give me a bit of desire energy. Come here, and I'll take care of the rest.'";
		otherwise:
			if the current brevity is skipall:
				say "(The gate guard is sated.)";
			otherwise:
				say "You try to stammer out another question. The guard grins. 'I know what you want to say. You want more, don't you? Well, I've had my fill for now. But I'm sure you'll find plenty of others who will be more than happy to give you a taste."

Instead of giving desire energy to the guard:
	If the guard is sated:
		continue the action;
	If the guard is unmet:
		say "Maybe you should ask first.";
	otherwise if the amount of desire energy is less than 1:
		say "It shouldn't be possible for you to not have enough energy here. I'll give you some, but probably let the dev know.";
		now the amount of the desire energy is 1;
	otherwise:
		if the current brevity is skip or the current brevity is skipall or (the current brevity is once and the guard is interacted):
			say "(You give 1 desire energy and the gate guard opens the gate for you.)";
		otherwise:
			say "You walk up to the guard, feeling quite unsure of yourself. 'So I just need to, um...'[paragraph break]The guard slips her arms around you, one hand settling on your rear and squeezing as she pulls you close. You let out a little surprised sound as your body squishes against hers. You can feel your stiff dick press up against her thigh. 'Just relax. All you've gotta do is let me feel you up.' She presses her thigh between your legs, rubbing her bare skin against your dick. 'Hehe...just imagine how your dick would feel in my pussy. Bet that'd feel nice, wouldn't it?[paragraph break]You let out a hot breath, rubbing yourself against her thigh a little. 'Ah...yeah...' you say, nodding along.[paragraph break]'That's a good girl. You're not gonna get any, but I do want you to think about it~' She slides her hands up to either side of your head and pulls you close, pressing a heated kiss to your lips. Just as you feel like you're about to cum against her thigh, she pulls back and grins. 'Mmm...I love the taste of needy little things like you. The taste of your need, your desire...that's the energy I was craving.[paragraph break]You pant softly, your dick swollen and leaky. 'Oh...gosh...w-will you be my girlfriend?' you blurt out clumsily.[paragraph break]The guard laughs. 'First time with a succubus, huh? Listen, you're cute, but I'm not looking to get entangled with a mortal.' She grabs her spear and taps the wall with its tip. The gate raises with a clanking, groaning sound. 'Go on. Maybe you'll find the girl of your dreams in the city. Not like you have much choice anyway...I assume you wanna get back home, and there's no going back the way you came.'";
		Now the iron gate is open;
		Decrease the amount of the desire energy by 1;
		Now the guard is sated;
		Now the guard is interacted.

Chapter 2 - Two Keys

Dark clearing is a resting spot. The printed name of the dark clearing is "Dark Clearing". The description of the dark clearing is "Beyond the gate, you find yourself in some kind of forest. Underground...? The trees must be quite different in Hell. They have dark blue bark, and you can see their roots tearing deep into the rock. Black leaves form a canopy overhead, dimming the ambient glow. There's a wide open clearing here, with just a couple paths through the tightly-grouped trees. Off to the side, a big, comfy hammock has been hung between two trees.[paragraph break](You can rest here! Resting recovers your energy, but everything will be moved back to its original place while you're asleep, except for items tucked away in the box here. Try storing something in the box if it looks like there's no other way to proceed.)[paragraph break]To the north is a quiet little hut. To the east is a wooden door wedged into the path with the words 'TRAINING AREA' splashed across it in red paint. To the west is the path back through the iron gate."

The canopy sky is a backdrop. It is in the dark clearing, meandering path, and training area. Understand "night sky" as the canopy sky. Understand "ceiling" as the canopy sky. Understand "leaves" as the canopy sky. The description of the canopy sky is "The dense black leaves form a canopy that mostly blocks your view of the sky. It gives you a bit less vertigo when you look straight up, although you can still see glimpses of it through the gaps in the leaves. Despite the strangeness of the local plant life, there's something comfortable here, almost sheltering; at least compared to the stark, yawning abyss of the cavernous ceiling."

The trees are a backdrop. They are in the dark clearing, meandering path, and training area.  Understand "tree" as the trees. The description of the trees is "They seem a lot tougher than your average tree. And you know, when you think about it, regular trees are actually pretty tough. The deep blue bark looks solid and thick, and the roots of the trees have ripped through the stone itself[emdash]seeking what, you can hardly imagine. You wonder briefly if these things are even trees at all, or some other organism entirely that just closely resembles them. Surely no real tree could thrive rooted in hard stone in a sunless cavern."

The hammock is scenery in the dark clearing. The description of the hammock is "A simple hammock made of a soft, comfortable material, tied between two trees. It's quite large and broad, and some visible wear and tear indicates that it's probably been used a lot."

The hut is a backdrop. It is in the dark clearing and meandering path. The description of the hut is "A squat little round hut sitting in the middle of the forest. It almost looks like it's out of a fairy tale, which is charming enough, but usually the heroes in fairy tales get to tackle their problems with clothes on. Hopefully there's no witch here who wants to eat you...or, um...well, maybe that [italic]is[normal] what you want."

The little storage box is a safe place in Dark clearing. The little storage box is fixed in place. The carrying capacity of the little storage box is 1. The description of the little storage box is "A neat little blue box tucked by the base of one of the trees the hammock is suspended from. It doesn't look like there's room for much inside."

Quiet hut is a room. The printed name of the quiet hut is "Quiet Hut". Quiet hut is north of Dark clearing. The description of the quiet hut is "While it might be fair to call the hut a very small home, it basically consists of one room, so maybe it's more optimistic to call it quite a large room. There's a bed on each side of the hut; one side has various weapons and wood carvings scattered about; the other has stacks of books and a writing desk with neatly organized writing supplies on it.[paragraph break]There are two entrances, the front entrance to the south and the back entrance to the north."

The beds are scenery in the quiet hut. Understand "bed" as the beds. The description of the beds is "A pair of simple beds. The one the quiet reader is lying in is neatly made, while the other is more disorganized and also has bits of wood shavings on it here and there, which strikes you as probably fairly uncomfortable."

The stacks of books are scenery in the quiet hut. Understand "book" as the stacks of books. Understand "stack" as the stacks of books. The description of the books is "Various books of all shapes and sizes. When you try to get a look at the titles, you find that it's all written in a language you don't even recognize. Your mind lights up with curiosity at the same moment you get the sinking realization that it's going to be pretty hard to learn about what sorts of books demons read. Not unless you end up staying down here long enough to learn the language."

A thing called weapons and wood carvings is in the quiet hut. The weapons are scenery. The description of the weapons is "A haphazard collection of various objects, many of which are pointy and some of which are not. Various swords and spears sit alongside wooden carvings of animals and other odds and ends. It's possible there's something useful in the pile, but it'd probably be dangerous to root around in. It's not your stuff anyway."

Instead of taking the weapons:
	say "You'd probably get a cut trying to reach into the pile. Probably for the best to leave it alone."

The writing desk is scenery in the quiet hut. The description of the writing desk is "A neat little writing desk that displays impressive craftsmanship, considering it was crafted from that iron-hard blue wood. It obviously belongs to the quiet reader, and it has all sorts of writing supplies on it, but I'm not going to tell you what any of them are. You're just going to try and take them, and you don't need them, I promise. Just leave her things alone."

The quiet reader is a woman in the quiet hut. "A quiet reader is in one of the beds, lying on her [if the quiet reader is unmet]belly[otherwise]back[end if] and quite absorbed in a book." The description of the quiet reader is "A tall, plump draconic figure with dark red skin. Her body is curvy and soft, and she has broad, leathery wings, and curved black horns atop her head. She doesn't wear a scrap of clothing, allowing you to see her heavy breasts, nicely rounded rear, and the rather large dick tucked between her legs. She's lounging in bed, reading a book and ignoring you."

The book of erotica is a thing carried by the quiet reader. The description of the book of erotica is "A hardbound book with rather explicit art of two lovers passionatedly entangled with each other on the cover. You're not sure what it's for yet, but when you think of the quiet reader, you feel like you should hang onto it."

Instead of talking to the quiet reader:
	if the quiet reader is unmet:
		if the current brevity is skipall:
			say "(The quiet reader wants 1 pleasure energy in exchange for the wooden key.)";
		otherwise:
			say "You clear your throat loudly. The woman ignores you and continues reading. You shuffle your feet awkwardly. Finally you manage to speak up. 'E-excuse me?'[paragraph break]The woman doesn't look up from her book. 'Yes?'[paragraph break]'I, um...I'm from Earth, and I need help getting home.'[paragraph break]She gives you enough attention to at least pause her reading. 'Oh, well then. I suppose you'll need the key to the back gate. I don't have it, though. I think my brother does. He's probably locked himself away in his little training area, so he isn't interrupted while he waves that sword around. I do have the key for [italic]that[normal] door, but I can taste your want for me in the air and it's [italic]terribly[normal] distracting.' She rolls onto her back, shifting her wings to spread out on either side of her. She holds her book open above her head in one hand, while she starts to stroke her stiff cock with the other. 'If only there was [italic]something[normal] we could do about that.'[paragraph break]Your cheeks flush as you stare at her cock. 'Oh, well...m-maybe we could think of something.'[paragraph break]She moves her book away from her face to finally look at you, offering you a wry smile. 'Well, you think about it. Either way, I'm [italic]quite[normal] enjoying this story.'[paragraph break](It will cost 1 pleasure energy to relieve the quiet reader's distraction.)";
		now the quiet reader is met;
	otherwise:
		if the quiet reader is unsated and the wooden key is carried by the quiet reader:
			if the current brevity is skipall:
				say "(The quiet reader wants 1 pleasure energy in exchange for the wooden key.)";
			otherwise:
				say "She pointedly gives her dick a few firm strokes. 'Mmm, [italic]so[normal] distracted.' she says to no one in particular.[paragraph break](It will cost 1 pleasure energy to relieve the quiet reader's distraction.)";
		otherwise:
			if the current brevity is skipall:
				say "(The quiet reader is sated.)";
			otherwise if the book of erotica is carried by the quiet reader:
				say "The quiet reader seems satisfied, and has resumed ignoring you.";
			otherwise:
				say "The quiet reader is happily humming to herself as she reads her book and ignores you."

Instead of giving pleasure energy to the quiet reader:
	If the quiet reader is sated or the wooden key is not carried by the quiet reader:
		continue the action;
	otherwise if the quiet reader is unmet:
		say "Maybe you should ask first.";
	otherwise if the amount of pleasure energy is less than 1:
		say "Not enough energy.";
	otherwise:
		if the current brevity is skip or the current brevity is skipall or (the current brevity is once and the quiet reader is interacted):
			say "(You give 1 pleasure energy and the quiet reader gives you the wooden key.)";
		otherwise:
			say "Well, she's made it...fairly clear what she wants, you think. You climb onto the bed, and although she doesn't acknowledge you, she does obligingly spread her legs and stops stroking herself to let you take over. You bring your face up to the tip of her penis, taking a moment to simply admire it. 'I've, um, never done this before...'[paragraph break]'It's the thought that counts. Now think very hard about sucking my dick.'[paragraph break]'Yes ma'am.' You dip your head down and run your tongue from base to tip. A pleasant shiver runs through her body. Well, maybe this won't be so hard...surely succubi are very sensitive, right? You wrap your lips around the head and slide slowly down her shaft, your tongue finding the underside and caressing along it. You start to bob carefully up and down, offering as much pleasure as you can with your inexperienced lips and tongue. She doesn't make much noise, but you can feel her cock flexing between your lips. You start to relax a little...she's still practically ignoring you, so it's almost like a practice dick. No expectant staring, no pressure. You try taking her dick deeper and find that you start to gag a little. Okay, not quite yet...you settle into an eager rhythm, sucking firmly as you bob your head. Finally, you hear a sound of pleasure escape her lips, as her dick swells, then pumps firmly, filling your mouth up with cum. You try to swallow down as much as you can, until it gets to be too much; you cough, and her cum spills from your mouth as the last of her load spurts onto your face.[paragraph break]She lets out a satisfied sigh, her dick softening. 'There you go, that wasn't too bad. Now why don't you clean up. I think there's a towel by the foot of the bed.'[paragraph break]'Yes, ma'am!' You fish around on the floor for the towel. Quickly locating it, you wipe off your face, then carefully towel off her crotch and the sheets. While you're focusing on that, you feel her hand settle between your ears and scritch softly.[paragraph break]'That's a good girl. Here.' She opens her hand in front of you, offering you the wooden key. 'And you can just drop the towel where you found it. I'll make my brother go clean it later.'[paragraph break]You gratefully take the key and drop the towel on the floor. 'Thank you! Um...do you, w-would you like to be my girlfriend?'[paragraph break]She hides her face behind her book, but you could swear her expression broke into a smile before she could stop herself. 'I don't have the attention to devote to a partner. Unless you like the arrangement where you live here and suck my dick, and I mostly ignore you.'[paragraph break]You think about it. Maybe that's not the kind of girlfriend you want long-term, and you'd like to eventually go home anyway. 'No, but thank you for the key. And the, um...th-thank you for the key.'";
		Now the wooden key is carried by the player;
		Decrease the amount of the pleasure energy by 1;
		Now the quiet reader is sated;
		Now the quiet reader is interacted.

Every turn when the player is in the quiet hut and the black key is carried by the player and the book of erotica is not carried by the player:
	say "The quiet reader glances in your direction as you walk by. 'Hey, before you go, I wanted to give you something.'[paragraph break]You jump a little, not having expected her to react to your presence. 'O-oh?'[paragraph break]She leans over the side of her bed and sifts through her stacks of books. 'Hold on, it should be here.' After a couple minutes of searching, she plucks one from the stacks and offers it to you.[paragraph break]You look at the cover of the book and your cheeks grow quite warm. 'Gosh, thank you. I'll be sure to read it when I get home.'[paragraph break]The reader grins. 'Oh, it's a great read, for sure. But you might find another use for it when you get home. I just...' She looks away, appearing bashful for the first time since you met her. 'I thought about it, and dating a girl as cute as you might be nice, actually. But...I know you need to go back to your world first.'[paragraph break]You tilt your head slightly, looking at the book. 'Alright. I'll hold onto it, then. And, um, thanks. I think you're cute too.'[paragraph break]She's already paying attention to her book again, but you can see a hint of a smile on her face.";
	now the book of erotica is carried by the player.

Training area is a room. The printed name of the training area is "Training Area". The description of the training area is "Beyond the wooden door is a smaller clearing. A couple of training dummies have been placed here, propped up on sticks that have been planted in holes cut into the stone ground.[paragraph break]To the west is the dark clearing."

The training dummy is scenery in the training area. Understand "dummies" as the training dummy. The description of the dummy is "Training dummies carved from the wood of the blue trees, in the rough shape of humanoid torsos on sticks. They're covered in sword notches, although they've fared surprisingly well considering how hard the swordfighter is hacking at them."

The swordfighter is a man in the training area. "A swordfighter is here, [if the swordfighter is met and the swordfighter is unsated]facing you expectantly[otherwise]hacking inelegantly at one of the training dummies with a heavy sword[end if]." The description of the swordfighter is "A tall, slender draconic figure with dark red skin. His body is toned and well-muscled, and he has broad, leathery wings, and curved black horns atop his head. He doesn't wear a scrap of clothing except the same pouch-laden belt as the gate guard had, giving you a nice view of his flat, muscular chest, and the wide vulva nestled between his strong thighs. He carries a sword that looks like it must be quite heavy, though he swings it like it weighs practically nothing."

The sword is a thing carried by the swordfighter. The description of the sword is "A rather hefty weapon, looking more like a cleaver sharpened from a big slab of steel than a traditional sword. Seeing him swing it full force into the training dummy and only leave a small nick, you wonder how he ever carved anything out of that blue wood in the first place."

Instead of talking to the swordfighter:
	If the swordfighter is unmet:
		if the current brevity is skipall:
			say "(The swordfighter wants 1 pleasure energy in exchange for the black key.)";
		otherwise:
			say "Having learned your lesson from the reader, you assert yourself a bit more loudly this time. 'Um, hey!'[paragraph break]The swordfighter glances at you, but doesn't stop hacking at the training dummy. 'Hey!' he answers back. 'Did my sister let you in here? I swear, she never gives me a moment's peace.'[paragraph break]Thinking back to the woman reading very quietly in bed, you're not sure you believe that's true, but it'd be more diplomatic not to say so. 'Um, sorry. She said you had the key to the back gate.'[paragraph break]He finally pauses and turns to you, planting his sword point-first against the ground and wiping his brow. You can get a better look at him from the front now, and while you're not [italic]romantically[normal] interested in boys, well...there's certainly something to be said for his smooth, muscular physique, as naked as the others. You try not to stare at his pussy too much...or should you? They are sex demons, after all. You wish you could be a bit more modest about your own arousal, but, well...it's just something you'll have to get comfortable with. 'Oh, the key?' he says. A broad grin spreads over his face. 'Oh, you don't have to be modest. You want something else too, don't you?'[paragraph break]You fidget and stammer a bit. 'I, ah...w-well...I guess nobody here is shy about sex, so I don't really have to be embarrassed about it...'[paragraph break]'Course not. Now why don't you get on your knees and bury your face against my pussy, and then we'll talk about that key.'[paragraph break]'Ah! Y-yes, sir!'[paragraph break](It will cost 1 pleasure energy to satisfy the swordfighter.)";
		now the swordfighter is met;
	otherwise:
		if the swordfighter is unsated and the black key is carried by the swordfighter:
			if the current brevity is skipall:
				say "(The swordfighter wants 1 pleasure energy in exchange for the black key.)";
			otherwise:
				say "'You ready yet? Come on, let's do this. I wanna see you on your knees, cute stuff.'[paragraph break]You blush and quickly nod. 'Yes sir!'[paragraph break](It will cost 1 pleasure energy to satisfy the swordfighter)";
		otherwise:
			if the current brevity is skipall:
				say "(The swordfighter is sated.)";
			otherwise:
				say "The swordfighter glances at you and says, 'Wanna train with me? I'm sure I could find a smaller sword somewhere.'[paragraph break]You shake your head. 'Um, no thanks.'"

Instead of giving pleasure energy to the swordfighter:
	If the swordfighter is sated or the black key is not carried by the swordfighter:
		continue the action;
	otherwise if the swordfighter is unmet:
		say "Maybe you should ask first.";
	otherwise if the amount of pleasure energy is less than 1:
		say "Not enough energy.";
	otherwise:
		if the current brevity is skip or the current brevity is skipall or (the current brevity is once and the swordfighter is interacted):
			say "(You give 1 pleasure energy and the swordfighter gives you the black key.)";
		otherwise:
			say "You take a deep breath and walk up to him, kneeling down on the hard stone ground. You look up at him, seeing his powerful figure towering over you. He grins and rests a hand on the back of your head, pressing your muzzle into his crotch. 'No need to be shy. Get your tongue right in there.'[paragraph break]'I hope it's okay I'm kinda new at this.' you say, slightly muffled against his vulva.[paragraph break]'Only way to get better is practice! I can already taste how eager you are, so get that tongue working.'[paragraph break]You nod, which also happens to rub your face against his pussy some more. You extend your tongue and start running it along his folds, stealing glances at him to gauge his reaction. He urges you on with another press on the back of your head, so you close your eyes and try to stay focused. You've never really gotten up-close with a pussy before, so you spend some time just exploring with your tongue, along the folds and contours. You find his clit with the tip of your tongue and hear him let out a hot breath. You make a pleased humming sound and lavish more attention on it, coaxing more aroused sounds from him.[paragraph break]'Yeah, that's it...' he murmurs. 'Just...keep that up.'[paragraph break]You nod and press in, your snout nosing against his firm pubis as you lap at his clit. You can sense his muscles tensing as you keep it up, until finally he lets out a sharp moan as his vulva twitches and convulses against your mouth. You keep lapping until he pushes your head back, panting hotly. You sit down on your butt, staring at his twitching folds. 'Oh...was that good?'[paragraph break]He smirks. 'Oh yeah. You're a natural at eating pussy. Here.' He fishes a black key out of one of his belt pouches and tosses it to you. 'Here you go. Good luck with...uh, why'd you need it again?'[paragraph break]'Oh, I need to find a way back to Earth.'[paragraph break]'Right. That makes sense. See you around, then.'";
		Now the black key is carried by the player;
		Decrease the amount of the pleasure energy by 1;
		Now the swordfighter is sated;
		Now the swordfighter is interacted.

The wooden key is a puzzle item carried by the quiet reader. The wooden key belongs to the quiet reader. The description of the wooden key is "It looks like even the key is made of wood. A lot of care has been put into crafting it; a lot more than the door, it seems like. Keys are just held to much higher standards, you suppose. The wood of those blue trees is as tough as iron, so there's no need to worry that it might break in the lock."

The wooden door is east of the dark clearing and west of the training area. The wooden door is a locked closed lockable door. The matching key of the wooden door is the wooden key. The wooden door is scenery. The description of the wooden door is "Clearly the result of amateur woodwork, but quite sturdy, and large enough to fill the gap between the trees. The adjacent trees have clearly suffered some violence to coerce them into serving as a doorframe. Some kind of latch is serving as a lock, with a visible keyhole. The words 'TRAINING AREA' are splashed across the door in red paint."

Meandering Path is a room. The printed name of the meandering path is "Meandering Path". The meandering path is north of the quiet hut. The description of the meandering path is "The path grows narrower beyond the hut, winding lazily between the trees. It starts to look like it might close off entirely, until finally it opens up at what looks like the edge of the forest.[paragraph break]The path further north is blocked by a wrought iron black fence, with an elegant-looking gate barring the way. The path south goes back towards the quiet hut."

The fence is scenery in the meandering path. The description of the fence is "A rather intricately-made wrought iron black fence that stretches from one end of the path to the other. It's topped with some pretty sharp-looking spikes, because of course it is. Maybe with a pair of tough jeans on you could clamber over the fence, but with all your soft parts dangling out it seems like a stupendously bad idea."

The black key is a puzzle item carried by the swordfighter. The black key belongs to the swordfighter. The description of the black key is "An elegant little black key made of iron. Maybe...too elegant. Does this key think it's better than you? No...it's just a key."

The black iron gate is north of the meandering path and south of the cliffside. The black iron gate is a locked closed lockable door. The black gate is scenery. The matching key of the black iron gate is the black key. The description of the black iron gate is "An elegant black gate made of iron, with a delicately curved frame and two doors whose bars form elaborate flower patterns. If you're hoping all this elegance and delicateness means that it's any less sturdy, I'm afraid that it's still made of quite unyielding iron bars."

The items of dark clearing is {the wooden key, the black key}. The doors of the dark clearing is {the wooden door, the black iron gate}. The locals of the dark clearing are {the quiet reader, the swordfighter}. The desire amount of the dark clearing is 1. The orgasm amount of the dark clearing is 1. The pleasure amount of the dark clearing is 1. The message of the dark clearing is "Hopefully no one minds if you take a quick nap here. You flop into the hammock, and you find it to be very comfy and soft. Within seconds, you're drifting off into a peaceful rest."

Chapter 3 - Witch's Cave

Cliffside is a room. The printed name of the cliffside is "Cliffside". The description of cliffside is "Beyond the forest, you find yourself on a rugged cliffside path that winds off into the distance. The purple-tinged sky yawns overhead, and the mountainside towers over you, the peak only faintly visible in the distance. Below the cliff, all you can see is a black, inky darkness, so dense and impenetrable that it looks more like a physical substance than a lack of light.[paragraph break]The path extends off to the north, and back towards the forest to the south. You can see the entrance to a cave to the east."

The darkness is a backdrop. It is in the cliffside, the cliff wall, the demon's road, and the tiny cave entrance. Understand "ocean" as the darkness. Understand "abyss" as the darkness. The description of the darkness is "A deep, black abyss, extending off into the distance. It's hard to tell if it's merely very dark down there, or if there's some kind of black, roiling ocean of darkness filling the void. Staring at it doesn't make it any easier to resolve the ambiguity, and also kinda makes you feel uncomfortable."

Witch's cave is a room. The printed name of the witch's cave is "Witch's Cave". The witch's cave is east of the cliffside. The description of the witch's cave is "A cozy cave that has been furnished into a comfy living space. The space is lit by the warm orange glow of a spherical glass bulb suspended from the ceiling, which is filled with some sort of luminescent fluid. Shelves line the walls of the cave, filled with various potions and other odds and ends. An expansive bed sits off to the side, practically taking up the northern half of the cave. A large, bubbling cauldron dominates the southern half of the cave. You can't help but feel dwarfed by the contents of the cave, as if you'd shrunk slightly upon entering."

The glass bulb is a backdrop. The glass bulb is in the witch's cave and atop-witch. The description is "A perfectly spherical glass bulb suspended from the ceiling by a rope. It's full of some kind of opaque fluid that's glowing orange, illuminating the room with a warm, soft light."

The shelves are a backdrop. The shelves are in the witch's cave and atop-witch. The description is "The shelves are crowded with objects; many of them appear to be potions or ingredients tucked into bottles and jars of various sizes, as well as various other bits and bobs that you have more difficulty identifying." Understand "potions" as the shelves. Understand "odds and ends" as the shelves.

The witch's bed is a backdrop. The witch's bed is in the witch's cave and atop-witch. The description is "A large, fluffy bed, thick with sheets and cushions. It looks like you could climb in and just sink right down into it. Of course, it's not [italic]your[normal] bed, but maybe the witch will let you sleep in it."

The cauldron is a backdrop. The cauldron is in the witch's cave and atop-witch.. Understand "shrinking potion" as the cauldron. Understand "potion" as the cauldron. The description is "A broad, squat black cauldron, heated by a dim blue flame crackling at its base. The flame emits only a gentle warmth, despite clearly heating the cauldron quite effectively. Within the cauldron is a swirling rainbow concoction, merrily bubbling away."

[I'd like to make the witch nonbinary. I should revisit this and see if I can wrestle Inform 7 into compliance.]

The witch is a person in the witch's cave. "A witch is here, brewing something in their cauldron." The description of the witch is "A tall, fat salamander seated behind the cauldron. Their figure is round and soft, and they must have at least a foot on you, seeming to loom over you even from their seated position. Their skin is a pale yellow shade, which looks considerably warmer in the cave's orange glow. Their large, heavy breasts spill over their round tummy, displaying their wide, dark nipples. Between their expansive thighs is a plump, protruding vulva. You might catch a twinkle in their eye as they notice you looking, but they make a show of paying you no mind, focusing their attention on the contents of the cauldron."

The ladle is a thing carried by the witch. The description of the ladle is "A big ladle, clearly just the right thing for stirring bubbling potions and taking ginger sips to see if it turns you into a frog or not. At least, that's what it's used for in this particular case."

Cliff wall is a room. The printed name of the cliff wall is "Cliff Wall". The cliff wall is north of the cliffside. The description of the cliff wall is "The path ends abruptly here, stopping at a sheer rock wall. There's a crack at the base of the wall that looks like it might be small enough for a mouse to fit through, but for someone of your size, it appears to be a dead end."

The cracked wall is scenery in the cliff wall. Understand "crack" as the cracked wall. The description of the cracked wall is "The little crack in the wall is far too small for you to fit through. Maybe there's something really interesting on the other side, but there's no way for you to find out. What a shame."

Potion offered is a truth state that varies. Potion offered is false.

Instead of drinking the cauldron:
	if potion offered is false:
		say "That seems awfully rude, and you don't even know if it's safe to go drinking whatever's in there.";
	otherwise:
		say "You borrow the witch's ladle and scoop up some of the colorful potion. You blow on it to cool it off, and take a nervous little sniff. It smells unidentifiably fruity. You carefully sip just a little bit, then a little bit more. 'Uh, is that enough?'[paragraph break]The witch smirks. 'I think so. You'd best put the ladle down, soon it will be too heavy for you.'[paragraph break]Sure enough, the ladle slowly seems to be getting larger and heavier. You drop it into the cauldron and look around you. The world around you is rapidly growing bigger. The witch reaches down and easily lifts you up off the ground. 'Ahh, you are even more adorable now, small one!' They hug you against their breasts, which are starting to appear quite enormous, especially with your face pressed against them. Smaller and smaller, as the witch keeps you cradled against their chest, until finally they have you practically cupped in their hands, while you find stable footing on the curve of their round belly.[paragraph break]'Ah, you shrunk so wonderfully, small one. Now you are free to skitter and crawl like a tiny mouse. Until your next orgasm, then you will grow big again! Perhaps you will find someone on your way who will help you with that.'[paragraph break]You blush. 'Ah, thank you.' You realize that your small voice must be even smaller now. 'I mean, thank you!' you say again, more loudly. You glance around. Admittedly, you have quite a pleasant perch on the witch's gigantic body, but perhaps it's time to get going.";
		now the player is in atop-witch;

Instead of talking to the witch:
	say "You look up at the witch’s face[emdash]or, is their head even above yours? You look at them, anyway. 'H-hello?'[paragraph break]The witch finishes dropping a few pinches of blue powder into the cauldron before they answer. 'Hello, small one. What can I do for you?'[paragraph break]”;
	if the witch is unmet or (potion offered is false and the player has been in the cliff wall):
		if the witch is unmet:
			say "You glance at the very comfy bed out of the corner of your eye. 'This might be a lot to ask, but I was wondering if I could rest here.'[paragraph break]The witch grins broadly. 'My bed is always available for travelers looking to rest their tired feet. Ah, but I’m feeling drowsy myself, and this potion needs to simmer for a time. So please allow me to join you.' They eye you lustfully, licking their lips.[paragraph break]'Ah...' You feel your cheeks growing warm. 'I would like that, ma’am.'[paragraph break]The witch chuckles. 'Oh, I’m not a ma’am, exactly. I’m just a humble witch with no use for gender.' They wink. 'But I’m so pleased that you like my offer. Please do rest at your leisure.'";
			if potion offered is false and the player has been in the cliff wall:
				say "[line break]'Thank you, um, witch. Um, there was one other thing...'[paragraph break]'Yes, small one?'[paragraph break]";
			now the witch is met;
		if potion offered is false and the player has been in the cliff wall:
			say "'I'm looking for a way home, but the path just kind of ends...'[paragraph break]The witch grins knowingly. 'Ah yes. A wall with a little crack, but much too small for even a small one like yourself, yes?'[paragraph break]You nod meekly.[paragraph break]The witch stirs the cauldron with a big ladle. 'You're in luck. I just happen to be brewing a shrinking potion that will give you just the mouselike quality to squeak through that little gap.[paragraph break]You lean over the cauldron, looking at the rainbow swirl. 'Are you sure this is safe to drink?'[paragraph break]'Probably! That is for you to learn! Go on, drink as you please.'";
			now potion offered is true;
	otherwise:
		say "You blush and stammer a bit. 'Um, I just wanted to say that I think you're cute.'[paragraph break]The witch smiles. 'Why thank you, small one.'"
			
The heart-shaped crystal is a thing carried by the witch. Understand "heart" as the heart-shaped crystal. The description of the heart-shaped crystal is "A gently shimmering pink crystal given to you by the witch. It appears to have grown into a heart shape naturally, somehow. You're not sure what it's for yet, but when you think of the witch, you feel like you should hang onto it."

Instead of resting in the witch's cave when the witch is met:
	if the witch is not sated:
		say "You climb into the big fluffy bed and flop down on your back. It's so comfy and soft, even better than your bed back home. Almost immediately, drowsiness sets in, and you close your eyes, drifting off.[paragraph break]After a few minutes of rest, you feel the bed shift under the witch's weight as they climb in with you. A moment later and you feel the softness of their body pressing down on you, their soft breasts squishing into your face. You crack an eye open and see their smiling face peeking out over the top of their breasts (bereft of hat, because, you suppose, a hat is a slightly silly thing to wear to bed). 'Still comfy, small one? Am I not too heavy~?' You nod. You've never slept underneath someone before, but their body is just as soft and warm as the bed itself, and you feel like you could sleep for ages here, enveloped in softness. 'Gooood girl. Rest, sleep, let this friendly witch take care of you.' they murmur, stroking between your ears. You nuzzle against their breasts as you drift off to sleep once more.[paragraph break]At the edge of your consciousness, you feel a warm, wet pressure against your dick. You make a muffled pleasured noise against the witch's breasts as your shaft twitches needily against the sensation. You realize it must be the witch's vulva, rubbing up and down against your shaft. 'Mm, that's good, yes?' they whisper. 'I'll take care of small one's needs, all you need to do is rest and be comfortable.' You manage the slightest of nods before falling back asleep.[paragraph break]The gentle tug of pleasure rouses you once more. You can feel the witch's pussy enveloping your dick now, rocking and squishing their hips against yours. It's hard to believe that the feeling of their pussy wrapped around your dick isn't waking you fully, but you're soooo comfortable...in fact, your dick only feels more comfortable enveloped by their soft, warm folds. You smile as a heady mix of pleasure and comfort keep you at a lazy sort of half-wakefulness, until you slide back to the other side of sleep once again.[paragraph break]...feeling them bouncing more vigorously on your dick...[paragraph break]...soft breasts pressing against you while they stroke your head...[paragraph break]...heated whispering in your ear as you feel your pleasure growing...[paragraph break]";
		say ".........a sudden rush of sensation pulls you back to consciousness. You let out a muffled moan as that feeling pours out between your legs, your dick pumping in a powerful orgasm. Your head feels like it's filled with warm, fluffy static, your conscious thoughts not quite able to keep up with the sleepy pleasure. Your dick twitches several more times, and then a heavy blanket of post-orgasmic relaxation settles over your body. Finally, you sink fully into sleep.[paragraph break]When you wake again, the witch has returned to their cauldron. You feel like you could just lay here forever, but finally you haul yourself into a sitting position, rubbing your eyes. 'How long was I asleep?'[paragraph break]'Ohh, I don't keep good track of the time. But I think my pussy put you to sleep for a little while, small one.' they say with a flirty wink.[paragraph break]You blush deeply. 'Y-yes, I guess it did. Um...I need to find a way home, but maybe I could...see you again?'[paragraph break]The witch stands up and rummages through their shelves. They find a pink crystal, roughly heart-shaped. 'Take this home, small one. You will know what to do.' They take your hand, placing the crystal inside it.[paragraph break]You squeeze the crystal in your palm and nod. 'Th-thank you.' You climb out of bed, your legs feeling slightly wobbly.";
		now the witch is sated;
		now the heart-shaped crystal is carried by the player;
	otherwise:
		say "The witch declines to join you this time, so you simply enjoy a restful nap in their big, soft bed."

Chapter 4 - Shrinking Violet

[I cannot coerce Inform 7 into doing anything sensical with a room named "atop the witch" or "on top of the witch" or whatever. While it may be technically possible, it seems silly to bother considering the underlying name of a room basically never becomes relevant.]

Atop-witch is a room. The printed name of atop-witch is "Atop the Witch". The description of atop-witch is "You find yourself on top of the witch's soft, pale orange belly. Their breasts loom over your head, nipples as wide as dinner plates from your tiny perspective. Their face peeks over top of them, watching you with amusement. Their huge thighs stretch off into the distance, and if you peek over the edge of their belly, you can catch sight of their vulva, now a cavernous entrance. Not that you'd think of entering them like that...well, you might [italic]think[normal] about it, but now probably isn't the time. Beyond the witch's body, the sight of the now building-sized furniture is a bit overwhelming; the surface of the cauldron is now a bubbling lake, and the bed is a vast, fluffy plain you could play baseball on. Well, assuming you could play baseball on a very squishy surface.[paragraph break]It looks like you could, with care, climb down to the floor from here."

The witch's breasts are scenery in atop-witch. Understand "breast" and "nipple" and "nipples" as the witch's breasts. The description of the witch's breasts is "Their breasts are huge, soft orbs that loom over you, with wide, dark nipples that protrude outward just a little bit. Up close, you can see the texture of the witch's skin in great detail, the smooth, shiny surface displaying a visible bumpiness that wouldn't normally be quite so noticeable."

The witch's belly is scenery in atop-witch. Understand "tummy" as the witch's belly. The description of the witch's belly is "A soft, expansive surface that serves as a stable place to stand. Anytime you shift your footing the witch giggles a bit in response. Their belly button forms a soft little hole that you could easily stumble over if you're not careful, and it strikes you that accidentally tripping over someone's belly button is the kind of social faux pas it never even occurred to you could ever come up until just now."

The witch's face is scenery in atop-witch. The description of the witch's face is "From here, the witch's face is monumentally large; their lips curved into a gentle smile, their eyes vast, dark pools of amused curiosity. They seem to be rather enjoying the time you're spending perched on their body, but then again, it was [italic]their[normal] idea for you to drink the potion in the first place. Surely this is some fetish of theirs, and you're unsure if the thought puts you at ease or makes you feel more embarrassed."

The witch's thighs are scenery in atop-witch. Understand "thigh" as the witch's thighs. The description of the witch's thighs is "Vast, soft thighs extending off into the distance. You could probably walk along them if you wanted to, but you'd be worried about losing your footing."

The witch's vulva is scenery in atop-witch. The description of the witch's vulva is "Large, plush petals form the entrance between the witch's legs, shimmering with a visible wetness. Their vulva sits upon a plump mound that squishes outward quite visibly, parting their lips to form a wide, inviting entrance. Their large, round clit sticks out from their clitoral hood, swollen and deep pink with excitement."

Instead of entering the witch's vulva:
	say "Your heart pounds faster at the thought of such an intimate act, but you couldn't bring yourself to do such a thing, even with permission. Besides, you'd get all messy."

Understand "climb [direction]" as going. [Doesn't really make sense if the player types "climb east" or whatever, but it saves the effort of doing something more complicated, and probably nobody will do that anyway.]

Down from atop-witch is the floor of the cave. The printed name of the floor of the cave is "Floor of the Witch's Cave". The description of the floor of the cave is "From down here, everything in the cave looms over you. The witch themself is a towering giant, several stories high. Most everything is far too tall for you to get a good look at it, but from here you can see that there's a generous amount of space under the witch's bed.[paragraph break]From here you could go under the witch's bed to the north, or out the cave entrance to the west. You could also climb back up onto the witch, with a fair bit of effort."

North of the floor of the cave is under the witch's bed. The printed name of under the witch's bed is "Under the Witch's Bed". The description of under the witch's bed is "The light illuminating the cave casts a dim glow over the underside of the witch's bed. A thin layer of dust has collected here; it looks like it's been relatively undisturbed for a good while."

Bed dust is privately-named scenery in under the witch's bed. Understand "dust" as the bed dust. The description of the bed dust is "The layer of dust appears considerably thicker compared to your tiny size. It's pretty gross, actually."

The wooden fox toy is a rideable animal in under the witch's bed. "[if the wooden fox is seen]The wooden fox toy is here, standing at attention.[otherwise]There's some kind of wooden fox toy here, curled up with its snout tucked against its tail.[end if]". The description of the wooden fox is "A fox of the four-legged variety, carved from wood in immaculate detail, with just enough of a suggestion of fur to still leave its body smooth to the touch.[if the wooden fox is seen] It's standing up and eyeing you with curiosity, proving to be surprisingly animate for something made of wood.[otherwise] It appears at first to be merely an inanimate toy, but as you regard it, it uncurls itself with a fluidity of motion that you would've thought impossible for something made of wood. It stands up and yawns, regarding you with curiosity.[end if]"

Instead of mounting the unseen wooden fox:
	say "You wouldn't really expect to be able to get anywhere riding on a toy. And yet...something seems a bit unusual about it."

West of the floor of the cave is the demon's road. The printed name of the demon's road is "Demon's Road". The description of the demon's road is "The cliffside path now extends off a dizzying distance, seeming to stretch on forever. You have no idea how you're going to make it all the way back to the cracked wall on foot.[paragraph break]The path stretches on and on to the north, and back towards the forest to the south. You can see the entrance to the witch's cave to the east."

South of the demon's road is a dead end. The description of dead end is "Placeholder description." [Exists only so the "instead" clause is not circumvented by the fact that the player can't go south.]

Instead of going south from the demon's road:
	say "You'd hate to get lost in the forest while tiny, and you don't have any reason to go back there anyway, so you think better of it."

Instead of going north from the demon's road when the player is not carried by the wooden fox:
	say "You spend a couple minutes walking along the path, but your destination feels like it's not getting any closer, so you head back. If only there was some way to travel faster..."

Before going north from the demon's road by fox:
	say "The fox bounds off down the path, while you cling to it as best you can."

Before going south from the tiny cave entrance by fox:
	say "The fox bounds off down the path, while you cling to it as best you can."
	
North of the demon's road is the tiny cave entrance. The printed name of the tiny cave entrance is "Tiny Cave Entrance". The description of the tiny cave entrance is "What had once been a little crack in the wall is now a generously large entrance to a dark cave. You can see a pinprick of light at the far end.[paragraph break]The entrance to the cave yawns open in front of you to the north. The cliffside path stretches on and on to the south."

Instead of going south from the tiny cave entrance when the player is not carried by the wooden fox:
	say "There's no way you're going to even bother trying to go back the way you came on foot."

Instead of going north from the tiny cave entrance by fox:
	say "The fox shakes its head and looks back towards the witch's cave. It doesn't appear that the fox is willing to travel any further from its home."

Chapter 5 - A Submerged Temple

The temple bath is a room. The temple bath is north of the tiny cave entrance. South of the temple bath is nothing. The printed name of the temple bath is "Temple Bath". The description of the temple bath is "A spacious room made of smooth, tan-colored stone. Most of the room is taken up by a large, square bath filled with warm water; in fact, the room is so filled with water that it comes to about ankle height even outside the bath. Two statues flank the sides of the bath, depicting nude, feminine figures, each holding a water jug from which an unending stream of water pours. Lilies float here and there on the water's surface, slowly swirlng about the room as if by unseen currents.[paragraph break]Excess water flows down a staircase to the east."

The bathing priestess is in the temple bath. "A bathing priestess is here, washing herself beneath one of the statue's fountains." The description of the bathing priestess is "A chubby salamander with pale blue skin. Her skin is smooth and soft, visibly slick with the water spilling down her body. She's a bit pear-shaped, with a squishy belly and smallish breasts with perky little nipples. Her soft folds are tucked between a pair of thick thighs."

[A bit of fiddling to make sure the compiler doesn't confuse the room and the scenery.]
The bath-thing is privately-named scenery in the temple bath. Understand "bath" as the bath-thing. The description is "A wide, fairly shallow bath, large enough for many people to sit comfortably in. Two statues endlessly overfill the bath, filling the room ankle-deep with water and letting the excess flow down the stairs out of the room."

The statues are scenery in the temple bath. Understand "feminine" and "figures" and "figure" and "statue" as the statues. The description of the statues is "".

The Hall of Estyphos is a room. The Hall of Estyphos is east of the temple bath. The printed name of the Hall of Estyphos is "Hall of Estyphos". The description of the Hall of Estyphos is "".

The bubble charm is a thing. The description of the bubble charm is "A shimmering, transparent blue sphere hanging from a thin loop of chain. It looks and feels like it might be made of hollow glass, but it weighs almost nothing at all."

[I'm starting to wonder if I should just avoid using direction words in room names at all because it's very confusing. At the very least, using "east side of..." instead of "east of..." helps to confuse the compiler considerably less.]

The east side of the bubble bridge is a room. The east side of the bubble bridge is northwest of the Hall of Estyphos. The printed name of east side of the bubble bridge is "East Side of the Bubble Bridge".

The west side of the bubble bridge is a room. The west side of the bubble bridge is west of the east side of the bubble bridge. The printed name of west side of the bubble bridge is "West Side of the Bubble Bridge".

Going west from the east side of the bubble bridge is crossing the bubble bridge.
Going east from the west side of the bubble bridge is crossing the bubble bridge.

Instead of crossing the bubble bridge while the bubble charm is not carried by the player:
	say "You tap the edge of the bridge with a toe. The bubble beneath your foot simply pops, as insubstantial as, well, a bubble. It doesn't look like you can cross right now."

The high priestess's quarters is a room. The high priestess's quarters is west of the west side of the bubble bridge. The printed name of the high priestess's quarters is "The High Priestess's Quarters". The description of the high priestess's quarters is "".

The high priestess is a woman in the high priestess's quarters. The description of the high priestess is ""

The temple quarters is a room. The temple quarters is northeast of the Hall of Estyphos. The printed name of the temple quarters is "Temple Quarters".

The breath charm is a wearable thing carried by the player. The description of the breath charm is "A sparkling pink spiral seashell, hanging from a necklace of blue beads. The opening of the shell forms a wide, round shape that you could comfortably fit your mouth around.".

Going northeast from the Hall of Estyphos is traveling through the submerged corridor.
Going southwest from the temple quarters is traveling through the submerged corridor.

Instead of traveling through the submerged corridor while the breath charm is not worn by the player:
	say "You peek into the water, peering at the corridor beneath. It looks like it goes a long way, and you don't trust yourself to hold your breath for that long. Maybe there's another way through."

Before traveling through the submerged corridor while the breath charm is held by the player and the breath charm is not worn by the player:
	try wearing the breath charm.

Before traveling through the submerged corridor while the breath charm is worn by the player:
	say "You press the opening of the charm to your lips, trying to breathe from it. Sure enough, you feel cool, fresh air filling your lungs. Breathing comfortably, you slip into the water and swim to the other side. By the time you climb out, dripping wet, your arms feel kind of tired, but you made it through without too much trouble at all."

The chamber of offering is a room. The chamber of offering is east of the Hall of Estyphos. The printed name of the chamber of offering is "Chamber of Offering".

The distracted disciple is a woman in the chamber of offering.

The temple entrance is a room. The temple entrance is north of the Hall of Estyphos. The printed name of the temple entrance is "Temple Entrance".